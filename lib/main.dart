import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:upstreet_flutter_code_challenge/providers/album.dart';

import './screens/album_list.dart';

void main() {
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(create: (_) => AlbumProvider()),
    ],
    child: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Upstreet Flutter code challenge',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: const Color(0xff01046d),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: const AlbumList(),
    );
  }
}
