import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as https;
import 'package:upstreet_flutter_code_challenge/services/api.dart';

class AlbumProvider extends ChangeNotifier {
  List albumList = [];

  getAllPhotos() async {
    https.Response response = await AlbumApi().fetchAllPhotos();
    if (response.statusCode == 200) {
      var albumBody = response.body;
      albumList = json.decode(albumBody);
      albumList = albumList.reversed.toList();
      notifyListeners();
    }
    return response;
  }

  addPhoto(Map map) {
    albumList = albumList.reversed.toList();
    albumList.add(map);
    albumList = albumList.reversed.toList();
    notifyListeners();
  }

  editPhoto(Map map, int index) {
    albumList[index] = map;
    notifyListeners();
  }
}
