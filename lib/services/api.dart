import 'package:http/http.dart' as https;

const API_ALBUMS_PHOTOS =
    'https://jsonplaceholder.typicode.com/albums/1/photos';

// TODO:
// Create a function for fetching data from `API_ALBUMS_PHOTOS`

class AlbumApi {
  fetchAllPhotos() async {
    https.Response response = await https.get(API_ALBUMS_PHOTOS);
    return response;
  }
}
