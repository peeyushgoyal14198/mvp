import 'package:flutter/material.dart';

class Helpers {
  void navigateTo(BuildContext context, Widget child) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => child),
    );
  }
}
