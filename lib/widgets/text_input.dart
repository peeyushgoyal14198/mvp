import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class TextInputWidget extends StatefulWidget {
  final TextEditingController textEditingController;
  final String hintText;
  final Widget prefixIcon;
  final TextInputType keyboardType;
  final Function validator;
  final Function onChange;
  final List<TextInputFormatter> inputFormater;
  final int maxLines;
  final TextInputAction textInputAction;
  final Function onFieldSubmitted;
  final bool isSearchField;
  final bool showSearchIconBeforeHand;

  TextInputWidget(
      {Key key,
      @required this.hintText,
      @required this.textEditingController,
      @required this.prefixIcon,
      @required this.keyboardType,
      this.inputFormater,
      this.maxLines,
      this.onChange,
      this.validator,
      this.textInputAction,
      this.onFieldSubmitted,
      this.showSearchIconBeforeHand = false,
      this.isSearchField = false})
      : super(key: key);

  @override
  _TextInputWidgetState createState() => _TextInputWidgetState();
}

class _TextInputWidgetState extends State<TextInputWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
       width: MediaQuery.of(context).size.width / 1.1,
        decoration: BoxDecoration(
            color: Colors.grey[200], borderRadius: BorderRadius.circular(5)),
        child: TextFormField(
          keyboardType: widget.keyboardType,
          textInputAction:
              widget.textInputAction ?? TextInputAction.unspecified,
          onFieldSubmitted: (value) {
            if (widget.onFieldSubmitted != null) {
              widget.onFieldSubmitted();
            }
          },
          style: Theme.of(context).textTheme.bodyText1,
          autofocus: false,
          validator: widget.validator,
          maxLines: widget.maxLines ?? 1,
          controller: widget.textEditingController,
          inputFormatters: widget.inputFormater ?? [],
          onChanged: (value) {
            if (widget.onChange != null) {
              widget.onChange();
            }
          },
          decoration: InputDecoration(
            hintText: widget.hintText,
            border: InputBorder.none,
            prefixIcon: Padding(
                padding: EdgeInsets.only(left: 10, right: 10),
                child: widget.prefixIcon),
            fillColor: Colors.black,
            prefixIconConstraints: BoxConstraints(minWidth: 00, minHeight: 0),
          ),
        ));
  }
}
