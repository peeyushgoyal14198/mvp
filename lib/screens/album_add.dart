import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:toast/toast.dart';
import 'package:upstreet_flutter_code_challenge/models/album.dart';
import 'package:upstreet_flutter_code_challenge/providers/album.dart';
import 'package:upstreet_flutter_code_challenge/widgets/button.dart';
import 'package:upstreet_flutter_code_challenge/widgets/text_input.dart';

class AlbumAddScreen extends StatefulWidget {
  AlbumAddScreen({Key key}) : super(key: key);

  @override
  _AlbumAddScreenState createState() => _AlbumAddScreenState();
}

class _AlbumAddScreenState extends State<AlbumAddScreen> {
  TextEditingController title = new TextEditingController();
  TextEditingController url = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("New Album"),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            height: 20,
          ),
          TextInputWidget(
            textEditingController: title,
            prefixIcon: null,
            keyboardType: TextInputType.emailAddress,
            hintText: "Album Title *",
          ),
          Container(
            height: 20,
          ),
          TextInputWidget(
            textEditingController: url,
            prefixIcon: null,
            keyboardType: TextInputType.emailAddress,
            hintText: "Album Cover Url *",
          ),
          Container(
            height: 30,
          ),
          ButtonWidget(
            title: "Save",
            onTap: () {
              if (title.text != "" && url.text != "") {
                List list = Provider.of<AlbumProvider>(context, listen: false)
                    .albumList;
                // list = list.reversed.toList();
                AlbumModel albumModel = AlbumModel.fromJson(list[0]);
                albumModel.title = title.text;
                albumModel.url = url.text;
                albumModel.id = albumModel.id + 1;
                Provider.of<AlbumProvider>(context, listen: false)
                    .addPhoto(albumModel.toJson());
                Navigator.of(context).pop();
              } else {
                Toast.show("Please enter all fields", context,
                    duration: Toast.LENGTH_LONG);
              }
            },
          )
        ],
      ),
    );
  }
}
