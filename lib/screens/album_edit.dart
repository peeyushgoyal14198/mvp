import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:toast/toast.dart';
import 'package:upstreet_flutter_code_challenge/models/album.dart';
import 'package:upstreet_flutter_code_challenge/providers/album.dart';
import 'package:upstreet_flutter_code_challenge/widgets/text_input.dart';

class AlbumEditScreen extends StatefulWidget {
  AlbumModel albumModel;
  final int index;
  AlbumEditScreen({Key key, @required this.albumModel, @required this.index})
      : super(key: key);

  @override
  _AlbumEditScreenState createState() => _AlbumEditScreenState();
}

class _AlbumEditScreenState extends State<AlbumEditScreen> {
  TextEditingController title = new TextEditingController();
  TextEditingController url = new TextEditingController();

  @override
  void initState() {
    setState(() {
      title = new TextEditingController(text: widget.albumModel.title);
      url = new TextEditingController(text: widget.albumModel.url);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Id :${widget.albumModel.id}"),
        centerTitle: true,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          if (title.text == "" || url.text == "") {
            Toast.show("Please enter all fields", context,
                duration: Toast.LENGTH_LONG);
          } else {
            widget.albumModel.title = title.text;
            widget.albumModel.url = url.text;
            Provider.of<AlbumProvider>(context, listen: false)
                .editPhoto(widget.albumModel.toJson(), widget.index);
            Navigator.of(context).pop();
            Navigator.of(context).pop();
          }
        },
        child: Center(
          child: Icon(Icons.save, color: Colors.white),
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            height: 20,
          ),
          TextInputWidget(
            textEditingController: title,
            prefixIcon: null,
            keyboardType: TextInputType.emailAddress,
            hintText: "Enter Title *",
          ),
          Container(
            height: 20,
          ),
          TextInputWidget(
            textEditingController: url,
            prefixIcon: null,
            keyboardType: TextInputType.emailAddress,
            hintText: "Enter Url *",
          ),
          Container(
            height: 30,
          ),
        ],
      ),
    );
  }
}
