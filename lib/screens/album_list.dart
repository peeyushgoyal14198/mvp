import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:provider/provider.dart';
import 'package:upstreet_flutter_code_challenge/models/album.dart';
import 'package:upstreet_flutter_code_challenge/providers/album.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:upstreet_flutter_code_challenge/screens/album_add.dart';
import 'package:upstreet_flutter_code_challenge/screens/album_view.dart';
import 'package:upstreet_flutter_code_challenge/util/helper.dart';
// TODO:
// 1. Create a list view to display the album data from the fetching function in `api.dart`
// 2. The item of the list should contain the album's thumbnail and title

class AlbumList extends StatefulWidget {
  const AlbumList();

  @override
  _AlbumListState createState() => _AlbumListState();
}

class _AlbumListState extends State<AlbumList> {
  bool loader = false;
  bool isError = false;
  @override
  void initState() {
    getAllPhotos();
    super.initState();
  }

  getAllPhotos() async {
    setState(() {
      loader = true;
    });
    Response response =
        await Provider.of<AlbumProvider>(context, listen: false).getAllPhotos();
    if (response.statusCode != 200) {
      setState(() {
        isError = true;
      });
    }
    setState(() {
      loader = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<AlbumProvider>(builder:
        (BuildContext context, AlbumProvider albumProvider, Widget child) {
      List albumList = [];
      albumList += albumProvider.albumList;
      return Scaffold(
        appBar: AppBar(
          title: const Text('Album List'),
          centerTitle: true,
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Helpers().navigateTo(context, AlbumAddScreen());
          },
          child: Center(
            child: Icon(Icons.add, color: Colors.white),
          ),
        ),
        body: loader
            ? Center(
                child: CircularProgressIndicator(),
              )
            : isError
                ? Container()
                : buildListView(albumProvider, albumList),
      );
    });
  }

  Widget buildListView(AlbumProvider albumProvider, List albumList) {
    //  albumList = albumList.reversed.toList();
    return ListView.builder(
      itemCount: albumList.length,
      itemBuilder: (context, index) {
        AlbumModel albumModel = AlbumModel.fromJson(albumList[index]);
        return InkWell(
          onTap: () {
            Helpers().navigateTo(
                context,
                AlbumViewScreen(
                  albumModel: albumModel,
                  index: index,
                ));
          },
          child: Padding(
            padding: EdgeInsets.all(10),
            child: Card(
              elevation: 5,
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Padding(
                  padding: EdgeInsets.only(left: 10, right: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      CachedNetworkImage(
                        imageUrl: albumModel.url,
                        height: 80,
                        width: 80,
                        fit: BoxFit.contain,
                        progressIndicatorBuilder:
                            (context, url, downloadProgress) => Container(
                          color: Colors.grey[300],
                        ),
                        errorWidget: (context, url, error) => Icon(Icons.error),
                      ),
                      Container(
                        width: 10,
                      ),
                      Flexible(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(albumModel.title,
                                style: Theme.of(context).textTheme.bodyText2),
                            // Text(albumModel.id.toString(),
                            //     style: Theme.of(context).textTheme.bodyText2)
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
