import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:upstreet_flutter_code_challenge/models/album.dart';
import 'package:upstreet_flutter_code_challenge/screens/album_edit.dart';

import 'package:upstreet_flutter_code_challenge/util/helper.dart';

class AlbumViewScreen extends StatefulWidget {
  AlbumModel albumModel;
  final int index;
  AlbumViewScreen({Key key, @required this.albumModel, @required this.index})
      : super(key: key);

  @override
  _AlbumViewScreenState createState() => _AlbumViewScreenState();
}

class _AlbumViewScreenState extends State<AlbumViewScreen> {
  @override
  Widget build(BuildContext context) {
    TextTheme textTheme = Theme.of(context).textTheme;
    return Scaffold(
      appBar: AppBar(
        title: Text("Id :${widget.albumModel.id}"),
        centerTitle: true,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Helpers().navigateTo(
              context,
              AlbumEditScreen(
                albumModel: widget.albumModel,
                index: widget.index,
              ));
        },
        child: Center(
          child: Icon(Icons.edit, color: Colors.white),
        ),
      ),
      body: Padding(
        padding: EdgeInsets.only(left: 10, right: 10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 20,
            ),
            Center(
              child: CachedNetworkImage(
                imageUrl: widget.albumModel.url,
                height: 100,
                width: 100,
                fit: BoxFit.contain,
                progressIndicatorBuilder: (context, url, downloadProgress) =>
                    Container(
                  color: Colors.grey[300],
                ),
                errorWidget: (context, url, error) => Icon(Icons.error),
              ),
            ),
            Container(
              height: 20,
            ),
            Center(
              child: Text(
                widget.albumModel.title,
                style: textTheme.headline6,
                textAlign: TextAlign.center,
              ),
            )
          ],
        ),
      ),
    );
  }
}
